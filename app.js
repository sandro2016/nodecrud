const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const baseRouter = require("./routes/baseRouter");
const productRouter = require("./routes/productRouter");

const app = express();

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended: false}));

app.use(function(request, response, next){
    let now = new Date();
    let hour = now.getHours();
    let minutes = now.getMinutes();
    let seconds = now.getSeconds();
    let data = `${hour}:${minutes}:${seconds} ${request.method} ${request.url}`;
    console.log(data);
    next();
});

app.use("/product", productRouter);
app.use("/", baseRouter);

app.use(function(request, response, next){
    response.sendStatus(404);
});

mongoose.connect("mongodb://localhost:27017/products", {useNewUrlParser: true}, function(err) {
    if(err) return console.log(err);
    app.listen(3000, function(){
        console.log("Server is running..");
    });
});