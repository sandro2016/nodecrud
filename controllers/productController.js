const Product = require("../models/product");

exports.list = function(request, response) {
    Product.find({}, function(err, result){
        if(err) {
            console.log(err);
            return response.sendStatus(400);
        }
        
        response.render("../views/product/list.ejs", {products: result});
    });
}

exports.create = function(request, response) {
    if(request.method == "POST") {
        if(!request.body) return response.sendStatus(400);

        const name = request.body.name;
        const price = request.body.price;

        Product.create({name: name, price: price}, function(err){
            if(err) console.log(err);
        });

        return response.redirect("/product");
    }

    response.render("../views/product/create.ejs");
}

exports.update = function(request, response) {
    const id = request.params["id"];

    if(request.method == "POST") {
        if(!request.body) return response.sendStatus(400);

        const name = request.body.name;
        const price = request.body.price;

        Product.findByIdAndUpdate(id, {name: name, price: price}, function(err, result){
            if(err) console.log(err);
        });

        response.redirect("/product");
    } else {
        Product.findById(id, function(err, result){
            if(err) {
                console.log(err);
                return response.sendStatus(400);
            }

            response.render("../views/product/update.ejs", {product: result});
        });
    }
}

exports.delete = function(request, response) {
    const id = request.params["id"];

    Product.findByIdAndDelete(id, function(err, result){
        console.log(result);
        if(err) console.log(err);
    });

    response.redirect("/product");
}