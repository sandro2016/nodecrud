const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const productScheme = new Scheme(
    {
        name: {
            type: String,
            required: true,
            minlength: 3,
            maxlength: 20
        },
        price: {
            type: Number
        }
    },
    {
        versionKey: false
    }
);

module.exports = mongoose.model("Product", productScheme);