const express = require("express");
const productController = require("../controllers/productController");

const productRouter = express.Router();

productRouter.use("/create", productController.create);
productRouter.use("/update/:id", productController.update);
productRouter.use("/delete/:id", productController.delete);
productRouter.use("/", productController.list);

module.exports = productRouter;

